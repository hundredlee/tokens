package config

import (
	"gitee.com/hundredlee/tokens/erc20"
	"gitee.com/hundredlee/tokens/token"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
)

type TokenFunc func(address common.Address, caller bind.ContractCaller) token.TokenInterface

var TokenType = map[string]TokenFunc{
	"erc20": func(address common.Address, caller bind.ContractCaller) token.TokenInterface {
		return erc20.NewERC20Token(address, caller)
	},
	//"lrc-erc20": func(address common.Address, caller bind.ContractCaller) token.TokenInterface {
	//	return erc20.NewLRCERC20Token(address, caller)
	//},
	//"icx-erc20": func(address common.Address, caller bind.ContractCaller) token.TokenInterface {
	//	return erc20.NewICXERC20Token(address, caller)
	//},
	//"dgd-erc20": func(address common.Address, caller bind.ContractCaller) token.TokenInterface {
	//	return erc20.NewDGDERC20Token(address, caller)
	//},
}
