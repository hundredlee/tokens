package scheme

import (
	"github.com/ethereum/go-ethereum/common"
	"math/big"
)

type InputData struct {
	FromAddress common.Address
	ToAddress   common.Address
	Value       *big.Int
}
