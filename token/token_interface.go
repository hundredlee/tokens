package token

import (
	"gitee.com/hundredlee/tokens/scheme"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"math/big"
)

type TokenInterface interface {
	DealInputData(inputData string) (input *scheme.InputData, err error)

	GetDecimal() (uint8, error)
	GetBalance(opts *bind.CallOpts, address string) (*big.Int, error)

	GetAbi() *abi.ABI

	Approve(opts *bind.TransactOpts, _spender common.Address, _value *big.Int) (*types.Transaction, error)
}
